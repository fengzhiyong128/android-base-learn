package com.fzy.android.broadcastreceiver.learn.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import timber.log.Timber;

/**
 * @Description: 描述
 * @Author: fzy
 * @CreateDate: 2021/8/23 11:25 上午
 */
public class MyStaticBroadCastReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        Timber.d("静态注册的 接收到广播");
        Timber.d(intent.getAction());
        Timber.d(intent.getStringExtra("data"));
    }
}
