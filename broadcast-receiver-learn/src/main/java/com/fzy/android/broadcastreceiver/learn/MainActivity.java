package com.fzy.android.broadcastreceiver.learn;

import androidx.appcompat.app.AppCompatActivity;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.fzy.android.broadcastreceiver.learn.receiver.LocalReceiver;
import com.fzy.android.broadcastreceiver.learn.receiver.MyBroadCastReceiver;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import timber.log.Timber;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    @SuppressLint("NonConstantResourceId")
    @BindView(R.id.id_send_broadcast)
    Button sendBroadcast;

    @SuppressLint("NonConstantResourceId")
    @BindView(R.id.id_broadcastText_edit)
    EditText broadcastEdit;


    @SuppressLint("NonConstantResourceId")
    @BindView(R.id.id_send_broadcast_static)
    Button broadcastEditStatic;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Timber.plant(new Timber.DebugTree());
        ButterKnife.bind(this);
    }


    /***
     * 第一个intent：不多说指定intent，所有广播接收者的匹配规则
     *
     * 第二个receiverPermission：指定广播接收器的权限，一般自定义，不常用，可传null。
     *
     * 第三个resultReceiver：指定一个最终的广播接收器，相当于finally功能，不论优先级，最后都要接收一次广播，而这一次收到的广播为无序广播（可以在BroadcastReceiver中通过boolean orderedBroadcast = isOrderedBroadcast()方法验证），但是却可以通过getResultData等方法取得数据，这就是上面提到的特殊情况。
     *
     * 第四个scheduler：看英文没怎么看懂什么意思，一般传null。
     *
     * 第五个initialCode：指定一个code，一般传Activity.RESULT_OK。
     *
     * 第六个initialData：传一个字符串数据。对应的在BroadcastReceiver中通过String resultData = getResultData()取得数据；通过setResultData("优先级为3的setResultData的数据")修改数据，将数据传给下一个优先级较低的BroadcastReceiver；如果在优先级较高的BroadcastReceiver中没有使用setResultData修改数据，那么优先级较低的接收到的数据还是最原始的数据，即initialData的值。
     *
     * 第七个initialExtras：传一个Bundle对象，也就是可以传多种类型的数据。对应的在BroadcastReceiver中通过Bundle bundle = getResultExtras(false)取得Bundle对象，然后再通过bundle的各种get方法取得数据；通过setResultExtras()传入一个修改过的bundle，将该bundle对象传给下一个优先级较低的BroadcastReceiver；如果在优先级较高的BroadcastReceiver中没有使用setResultExtras修改数据，那么优先级较低的接收到的数据还是最原始的bundle对象，即initialExtras的值。
     *
     * 有序广播所对应的所有的receiver按照在intent-filter中设置的android:priority属性依次执行，android:priority表示优先级，值越大，其所对应的广播接收者，越先接收到广播。在android:priority相同的情况下，如果广播接收器是通过静态注册的，则接收到广播的顺序不确定，如果是动态注册的，先注册的将先收到广播。
     *
     * 有序广播可以被拦截，�可以在较高优先级的接收器中通过abortBroadcast()拦截广播，这样就会导致较低优先级的接收器无法收到广播了，但是sendOrderedBroadcast第三个参数指定的BroadcastReceiver还是会收到广播的，而且能获得数据。
     *
     * 有序广播可以通过原始intent.putExtra这种方式传递数据给BroadcastReceiver，也能通过sendOrderedBroadcast方法的最后2个参数传递数据，但是通过第一种方式传递的数据无法中途修改，通过第二种方式传递的可以通过上面参数说明中的方式进行修改。
     *
     * 作者：镜花水月一梦不觉
     * 链接：https://www.jianshu.com/p/0b3a7b35d76d
     * 来源：简书
     * 著作权归作者所有。商业转载请联系作者获得授权，非商业转载请注明出处。
     * @param v
     */
    @OnClick({R.id.id_send_broadcast,R.id.id_broadcastText_edit,R.id.id_send_broadcast_static,R.id.id_send_broadcast_local})
    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.id_send_broadcast:
                Intent intent = new Intent();
                intent.setAction(MyBroadCastReceiver.class.getPackage().toString());
                intent.putExtra("data",broadcastEdit.getText().toString());
                /**
                 * 不加第二个参数 为发送普通
                 */
                sendBroadcast(intent);
                sendOrderedBroadcast(intent,null);
                break;
            case R.id.id_send_broadcast_static:
                Intent intent2 = new Intent();
                intent2.setAction("com.fzy.android.broadcastreceiver.static");
                intent2.putExtra("data",broadcastEdit.getText().toString());
                /**
                 * 没有这句的话 是无法发送静态广播的
                 * 目前提倡使用动态注册的方式
                 */
                intent2.setPackage(getPackageName());
                sendBroadcast(intent2);
                break;
            case R.id.id_send_broadcast_local:
                Intent intent3 = new Intent();
                intent3.setAction("com.fzy.android.broadcastreceiver.local");
                intent3.putExtra("data",broadcastEdit.getText().toString());
                /**
                 * 没有这句的话 是无法发送静态广播的
                 * 目前提倡使用动态注册的方式
                 */
                intent3.setPackage(getPackageName());
                localBroadcastManager.sendBroadcast(intent3);
                break;
        }
    }

    private LocalBroadcastManager localBroadcastManager;
    LocalReceiver localReceiver;
    private void initLocalReceiver() {
        //获取实例
        localBroadcastManager = LocalBroadcastManager.getInstance(this);
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("com.fzy.android.broadcastreceiver.local");
        localReceiver = new LocalReceiver();
        localBroadcastManager.registerReceiver(localReceiver, intentFilter);
    }


    MyBroadCastReceiver myBroadCastReceiver;
    @Override
    protected void onResume() {
        super.onResume();
        initLocalReceiver();
        // 1. 实例化BroadcastReceiver子类 & IntentFilter
        myBroadCastReceiver = new MyBroadCastReceiver();
        // 2. 设置接收广播的类型
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.setPriority(1);
        intentFilter.addAction(MyBroadCastReceiver.class.getPackage().toString());
        // 3. 动态注册：调用Context的registerReceiver（）方法
        registerReceiver(myBroadCastReceiver,intentFilter);
    }
    @Override
    protected void onPause() {
        super.onPause();
        //销毁在onResume()方法中的广播
        unregisterReceiver(myBroadCastReceiver);
        localBroadcastManager.unregisterReceiver(localReceiver);
    }
}