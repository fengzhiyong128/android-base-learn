package com.fzy.android.broadcastreceiver.learn.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import timber.log.Timber;

/**
 * @ProjectName: AndroidBaseLearn
 * @Description: 自定义接收广播的类
 * @Author: fzy
 */
public class MyBroadCastReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {

        boolean orderedBroadcast = isOrderedBroadcast();

        if (orderedBroadcast) {
            Timber.d("动态注册的 接收到有序广播");
            Timber.d(intent.getAction());
            Timber.d(intent.getStringExtra("data"));
            //拦截广播
            Timber.d("拦截有序广播");
            abortBroadcast();
        } else {
            Timber.d("动态注册的 接收到广播");
            Timber.d(intent.getAction());
            Timber.d(intent.getStringExtra("data"));
        }
    }
}
