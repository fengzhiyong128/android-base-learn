package com.fzy.android.base.aidllearnclient;

import androidx.appcompat.app.AppCompatActivity;

import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.os.RemoteException;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.fzy.android.aidl.learn.server.ILearnAidlInterface;

public class MainActivity extends AppCompatActivity {
    ILearnAidlInterface iLearnAidlInterface;
    EditText nameEdit ;


    ServiceConnection service = new ServiceConnection()
    {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service)
        {
            Log.d("绑定服务",service.toString());
            iLearnAidlInterface = ILearnAidlInterface.Stub.asInterface(service);
        }

        @Override
        public void onServiceDisconnected(ComponentName name)
        {
            Log.d("绑定服务 ","onServiceDisconnected"+name);
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        nameEdit = findViewById(R.id.id_edit_name);

    }

    public void bindService(View view){
        Intent intent = new Intent();
        intent.setPackage("com.fzy.android.aidl.learn.server");
        intent.setAction("com.fzy.android.aidl.learn.server.action");
        ComponentName componentName = new ComponentName(
                "com.fzy.android.aidl.learn.server",
                "com.fzy.android.aidl.learn.server.AidlService");
        intent.setComponent(componentName);
        bindService(intent, service , BIND_AUTO_CREATE);
    }

    public void getNameFromServer(View view)
    {
        if (iLearnAidlInterface == null){
            Toast.makeText(MainActivity.this,"请先绑定服务", Toast.LENGTH_SHORT).show();
            return;
        }
        try
        {
            Toast.makeText(MainActivity.this, iLearnAidlInterface.getName(), Toast.LENGTH_SHORT).show();
            nameEdit.setText(iLearnAidlInterface.getName());
        }
        catch (RemoteException e)
        {
            e.printStackTrace();
        }
    }

    public void setNameForServer(View view)
    {
        if (iLearnAidlInterface == null){
            Toast.makeText(MainActivity.this,"请先绑定服务", Toast.LENGTH_SHORT).show();
            return;
        }
        try {
            iLearnAidlInterface.setName(nameEdit.getText().toString());
        } catch (RemoteException e) {
            e.printStackTrace();
        }

    }


    @Override
    protected void onDestroy() {
        unbindService(service);
        super.onDestroy();
    }
}