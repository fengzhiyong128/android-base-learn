package com.fzy.learn.nativelib

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log

class MainActivity : AppCompatActivity() {
    companion object{
        val TAG = "主类 日志 ";
    }

    val nativeLib = NativeLib()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main2)
        testJNI()
    }

    private fun testJNI(){
        Log.i(TAG, "testJNI------| : ${nativeLib.stringFromJNI()}")
        Log.i(TAG, "testJNI------| : ${nativeLib.stringFromJNI2(null)}")
        Log.i(TAG, "testJNI2: ${JNIClass.testMethod("123")}")
        Log.i(TAG, "testJNI3: ${JNIClass.testMethod2("123")}")
        Log.i(TAG, "testJNI4: ${JNIClass.testMethod2(null)}")
        Log.i(TAG, "testJNI5: ${JNIClass.testMethod2("")}")


        Log.i(TAG, "testJNI６: ${JNIClass.testMethod3("afdasfafd")}")
        Log.i(TAG, "testJNI７: ${JNIClass.testMethod3(null)}")

    }
}