package com.fzy.learn.nativelib;

/**
 * @Description: 描述
 * @Author: fzy
 * @CreateDate: 2022/3/21 5:15 下午
 */
public class JNIClass {

    public static native boolean testMethod(String info);

    /**
     * data 问题是 传入 null 但是c层的 (NULL == data) 无法达成 <font color=red>原因未知</font>
     * @param data
     * @return
     */
    public static native boolean testMethod2(String data);

    public static native String testMethod3(String data);
}
