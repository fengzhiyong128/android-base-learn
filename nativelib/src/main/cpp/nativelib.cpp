#include <jni.h>
#include <string>

extern "C" JNIEXPORT jstring JNICALL
Java_com_fzy_learn_nativelib_NativeLib_stringFromJNI(
        JNIEnv *env,
        jobject /* this */) {
    std::string hello = "Hello from C++";
    return env->NewStringUTF(hello.c_str());
}

extern "C" JNIEXPORT jstring JNICALL
Java_com_fzy_learn_nativelib_NativeLib_stringFromJNI2(
        JNIEnv *env,
        jobject /* this */, jstring jstring1) {
    if (NULL == jstring1) {
        std::string hello = "参数为空";
        return env->NewStringUTF(hello.c_str());
    }
//    std::string hello = "Hello from C++";
//    return env->NewStringUTF(hello.c_str());
    return jstring1;
}