////
//// Created by fzy on 2022/3/21.
////
//
//#ifndef ANDROIDBASELEARN_MYLOG_H
//#define ANDROIDBASELEARN_MYLOG_H
//#define _OS_ANDROID_
//
//#if defined _OS_ANDROID_
//#pragma message("LOG ON macro _OS_ANDROID_ defined ")
//        #include <android/log.h>
//        #include <string.h>
//        #include <stdio.h>
//        #define __filename(x) (strrchr(x,'/') ? strrchr(x,'/')+1:x)
//
//        #define LOGD(...)  EsLog(ANDROID_LOG_DEBUG, __filename(__FILE__), __LINE__,__VA_ARGS__)
//        #define LOGI(...)  EsLog(ANDROID_LOG_INFO, __filename(__FILE__), __LINE__,__VA_ARGS__)
//        #define LOGW(...)  EsLog(ANDROID_LOG_WARN, __filename(__FILE__), __LINE__,__VA_ARGS__)
//        #define LOGE(...)  EsLog(ANDROID_LOG_ERROR, __filename(__FILE__), __LINE__,__VA_ARGS__)
//        #define LOGF(...)  EsLog(ANDROID_LOG_FATAL, __filename(__FILE__), __LINE__,__VA_ARGS__)
//        #define LOGBYTEARRAY LogByteArray
//
//#endif //ANDROIDBASELEARN_MYLOG_H
//
