//
// Created by fzy on 2022/3/21.
//
// TODO 作者懒 不想写头文件 注册部分在最底下 自己看吧
#include <string>
#include <stdio.h>
#include <signal.h>
#include <memory.h>

#define LOG_TAG "JNI_DEBUG"

#include <android/log.h>
#include <jni.h>

#define LOGE(...) __android_log_print(ANDROID_LOG_ERROR,LOG_TAG,## __VA_ARGS__)
#define LOGI(...) __android_log_print(ANDROID_LOG_INFO,LOG_TAG, ## __VA_ARGS__)
#define LOGD(...) __android_log_print(ANDROID_LOG_DEBUG,LOG_TAG, ## __VA_ARGS__)


static JavaVM *s_pJavaVM = NULL;
static JNIEnv *s_pJniEvn = NULL;


/*--------------------------------------相当于头部分------------------------------------------*/
jint RegisterApiNatives(JNIEnv *pEnv);


/*--------------------------------------- 正文 ---------------------------------------------*/
jint JNI_OnLoad(JavaVM *vm, void *reserved) {
    jint result = -1;
    JNIEnv *env = NULL;

//	LOGI("JNI_OnLoad enters");
    if (vm->GetEnv((void **) &env, JNI_VERSION_1_6) != JNI_OK) {
//	    LOGI("JNI_VERSION error");
        goto error;
    }

    s_pJavaVM = vm;
    s_pJniEvn = env;

    LOGI("JNI_OnLoad running");
    result = RegisterApiNatives(env);
    if (JNI_OK != result) {
        goto error;
    }

    result = JNI_VERSION_1_6;
    error:
    LOGI("JNI_OnLoad error result = %d", result);
    return result;
}


static int MyRegisterNatives(JNIEnv *env, const char *clsName, const JNINativeMethod *nativeMethods,
                             unsigned int nativeMethodsCount) {
    int ret = 0x00;
    jclass cls = NULL;
    JNINativeMethod localNathiveMethod;
    jmethodID methodID;

    if (NULL == env || NULL == clsName || NULL == nativeMethods || 0x00 == nativeMethodsCount) {
        ret = JNI_ERR;
        goto END;
    }

    if (0x00 != ret) {
        LOGI("参数错误");
        goto END;
    }
    cls = env->FindClass(clsName);
    if (NULL == cls) {
        ret = JNI_ERR;
        goto END;
    }

    for (int i = 0x00; i < nativeMethodsCount; i++) {
        LOGI("第 %d 次, 注册方法为 %s 注册参数为 %s", i, nativeMethods[i].name, nativeMethods[i].signature);

        methodID = env->GetStaticMethodID(cls, nativeMethods[i].name, nativeMethods[i].signature);

        LOGI("方法 %s 的id是 %p", nativeMethods[i].name, methodID);

        if (NULL != methodID) {

            localNathiveMethod.name = nativeMethods[i].name;

            localNathiveMethod.signature = nativeMethods[i].signature;

            localNathiveMethod.fnPtr = nativeMethods[i].fnPtr;

            ret = env->RegisterNatives(cls, &localNathiveMethod, 0x01);

            LOGI("动态注册 %s 的结果 %d", nativeMethods[i].name, ret);

            if (0x00 != ret) {
                LOGI("%s 注册出错了 ", nativeMethods[i].name);
                goto END;
            }
        } else {
            env->ExceptionClear();
        }
    }

    END:
    return ret;
}

static int MyRegisterNatives2(JNIEnv *env, const char *clsName, const JNINativeMethod *nativeMethods,
                             unsigned int nativeMethodsCount) {
    int ret = 0x00;
    jclass cls = NULL;
    jmethodID methodID;

    if (NULL == env || NULL == clsName || NULL == nativeMethods || 0x00 == nativeMethodsCount) {
        ret = JNI_ERR;
        goto END;
    }

    if (0x00 != ret) {
        LOGI("参数错误");
        goto END;
    }
    cls = env->FindClass(clsName);
    if (NULL == cls) {
        ret = JNI_ERR;
        goto END;
    }

    for (int i = 0x00; i < nativeMethodsCount; i++) {
        LOGI("第 %d 次, 注册方法为 %s 注册参数为 %s", i, nativeMethods[i].name, nativeMethods[i].signature);

        methodID = env->GetStaticMethodID(cls, nativeMethods[i].name, nativeMethods[i].signature);

        LOGI("方法 %s 的id是 %p", nativeMethods[i].name, methodID);

        if (NULL != methodID) {

            LOGI(" 验证 %s 的结果 %d", nativeMethods[i].name, ret);

        } else {
            env->ExceptionClear();
        }
    }


    ret = env->RegisterNatives(cls, nativeMethods, nativeMethodsCount);
    if (0x00 != ret) {
        LOGI("注册出错了 ");
        goto END;
    }
END:
    return ret;
}

/*--------------------------------------------- 需要注册的函数 -----------------------------------------------*/

jboolean needRegistMethod(JNIEnv *env, jstring info) {
    if (NULL == info) {
        return JNI_FALSE;
    }

    return JNI_TRUE;
}

/**
 * 这个 jobject 做什么用的
 * @param env
 * @param data
 * @return
 */
jboolean needRegistMethod2(JNIEnv *env,jobject /* this */, jstring data) {
    if (nullptr == data ) {
        return JNI_FALSE;
    }

    return JNI_TRUE;
}
jstring needRegistMethod3(JNIEnv *env,jobject /* this */, jstring data) {
    if (NULL == data) {
        std::string hello = "参数为空";
        return env->NewStringUTF(hello.c_str());
    }
//    std::string hello = "Hello from C++";
//    return env->NewStringUTF(data);
    return data;
}

/*--------------------------------------------- 对函数进行注册 -----------------------------------------------*/

static const char *classPath = "com/fzy/learn/nativelib/JNIClass";

static JNINativeMethod gs_needRegestMethods[] =
        {
                {"testMethod",  "(Ljava/lang/String;)Z", (void *) needRegistMethod},
                {"testMethod2", "(Ljava/lang/String;)Z", (void *) needRegistMethod2},
                {"testMethod3", "(Ljava/lang/String;)Ljava/lang/String;", (void *) needRegistMethod3},

        };


jint RegisterApiNatives(JNIEnv *pEnv) {
    int result = MyRegisterNatives2(pEnv, classPath, gs_needRegestMethods,
                                   sizeof(gs_needRegestMethods) /
                                   sizeof(gs_needRegestMethods[0x00]));
    return result;
}
