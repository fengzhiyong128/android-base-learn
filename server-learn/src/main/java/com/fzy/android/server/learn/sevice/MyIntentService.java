package com.fzy.android.server.learn.sevice;

import android.app.IntentService;
import android.content.Intent;

import androidx.annotation.Nullable;

/**
 * @Description: IntentService 是一个基于Service的一个类，用来处理异步的请求。你可以通过startService(Intent)来提交请求，
 * 该Service会在需要的时候创建，当完成所有的任务以后自己关闭，而不需要我们去手动控制
 * @Author: fzy
 * @CreateDate: 2021/8/25 3:22 下午
 */
public class MyIntentService extends IntentService {

    /**
     * Creates an IntentService.  Invoked by your subclass's constructor.
     *
     * @param name Used to name the worker thread, important only for debugging.
     *             给线程设置名字
     */
    public MyIntentService(String name) {
        super(name);
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {

    }

}
