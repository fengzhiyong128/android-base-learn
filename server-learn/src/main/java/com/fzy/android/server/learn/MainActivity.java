package com.fzy.android.server.learn;

import androidx.appcompat.app.AppCompatActivity;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.fzy.android.server.learn.listener.OnProgressListener;
import com.fzy.android.server.learn.sevice.ServiceOne;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import timber.log.Timber;

public class MainActivity extends AppCompatActivity {

    ServiceOne serviceOne ;

    boolean bindState;

    @BindView(R.id.id_progress_bar)
    ProgressBar progressBar;

    ServiceConnection serviceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            Timber.d("绑定服务 %s",service.toString());
            Toast.makeText(getBaseContext(), "服务绑定成功", Toast.LENGTH_SHORT).show();
            serviceOne = ((ServiceOne.ServiceOneBinder)service).getServiceOne();
            if (serviceOne!=null){
                bindState = true;
            }
            serviceOne.setOnProgressListener(new OnProgressListener() {
                @Override
                public void onProgress(int progress) {
                    Timber.d("进度条%s", progress);
                    progressBar.setProgress(progress);
                }
            });
            serviceOne.startDownLoad();
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            Timber.d("绑定服务 onServiceDisconnected %s",name);
        }
    };
    Intent intentService;
    Unbinder unbinder;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Timber.plant(new Timber.DebugTree());
        unbinder = ButterKnife.bind(this);
        intentService = new Intent(this,ServiceOne.class);

    }


    public void startService(View view){
        startService(intentService);
    }

    public void bindService(View view){

        MainActivity.this.bindService(intentService,serviceConnection, BIND_AUTO_CREATE);

    }

    public void stopAndUnbindService(View view){
        if (bindState){
            unbindService(serviceConnection);
            stopService(intentService);
            bindState = false;
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unbinder.unbind();
        if (bindState){
            unbindService(serviceConnection);
            stopService(intentService);
            bindState = false;
        }
    }
}