package com.fzy.android.server.learn.listener;

/**
 * @Description: 反馈给进度条的接口
 * @Author: fzy
 * @CreateDate: 2021/8/25 3:38 下午
 */
public interface  OnProgressListener {
    void onProgress(int progress);
}
