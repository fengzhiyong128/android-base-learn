// ILearnAidlInterface.aidl
package com.fzy.android.aidl.learn.server;


// Declare any non-default types here with import statements

interface ILearnAidlInterface {

    String getName();

    void setName(String name);
}