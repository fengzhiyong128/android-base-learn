package com.fzy.android.aidl.learn.server;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * @Description: 描述
 * @Author: fzy
 * @CreateDate: 2021/8/25 9:54 上午
 */
public class TransInfo implements Parcelable {

    /**
     * 用户名
     */
    String userName;

    /**
     * 动态索引
     */
    String dynamicIndex;



    protected TransInfo(Parcel in) {
        userName = in.readString();
        dynamicIndex = in.readString();
    }

    public static final Creator<TransInfo> CREATOR = new Creator<TransInfo>() {
        @Override
        public TransInfo createFromParcel(Parcel in) {
            return new TransInfo(in);
        }

        @Override
        public TransInfo[] newArray(int size) {
            return new TransInfo[size];
        }
    };


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(userName);
        dest.writeString(dynamicIndex);
    }

    public void readFromParcel(Parcel dest) {
        userName = dest.readString();
        dynamicIndex = dest.readString();
    }

}
