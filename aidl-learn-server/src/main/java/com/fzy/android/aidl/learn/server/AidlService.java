package com.fzy.android.aidl.learn.server;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.os.RemoteException;
import android.widget.Toast;

import androidx.annotation.Nullable;

/**
* 
* @Description: 描述 
* @Author: fzy
* @CreateDate: 2021/8/25 10:16 上午
*/
public class AidlService extends Service {
    private String userName = "name";
    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return binder;
    }

    private final ILearnAidlInterface.Stub binder = new ILearnAidlInterface.Stub() {
        @Override
        public String getName() throws RemoteException {
            return userName;
        }

        @Override
        public void setName(String name) throws RemoteException {
            userName = name;
        }
    };

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        // Let it continue running until it is stopped.
        Toast.makeText(this, "服务已经启动", Toast.LENGTH_SHORT).show();
        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Toast.makeText(this, "服务已经停止", Toast.LENGTH_SHORT).show();
    }
}
