package com.fzy.android.aidl.learn.server;

import androidx.appcompat.app.AppCompatActivity;

import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;
import android.view.View;

public class MainActivity extends AppCompatActivity {
    Intent intentOne;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        intentOne = new Intent(this, AidlService.class);
    }
    public void startServer(View view){
        MainActivity.this.startService(intentOne);
    }

    public void stopServer(View view){
        MainActivity.this.stopService(intentOne);

    }

}