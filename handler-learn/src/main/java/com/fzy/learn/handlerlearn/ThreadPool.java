package com.fzy.learn.handlerlearn;

import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * @Description: 线程池生成类
 * @Author: fzy
 * @CreateDate: 2022/3/18 3:37 下午
 * -> corePoolSize 线程池中核心线程的数量
 * -> maximumPoolSize 线程池中最大线程数量
 * -> keepAliveTime 非核心线程的超时时长，当系统中非核心线程闲置时间超过keepAliveTime之后，则会被回收。
 * 如果ThreadPoolExecutor的allowCoreThreadTimeOut属性设置为true，则该参数也表示核心线程的超时时长
 * -> unit 第三个参数的单位，有纳秒、微秒、毫秒、秒、分、时、天等
 * -> workQueue 线程池中的任务队列，该队列主要用来存储已经被提交但是尚未执行的任务。
 * 存储在这里的任务是由ThreadPoolExecutor的execute方法提交来的。
 * -> threadFactory 为线程池提供创建新线程的功能，这个我们一般使用默认即可。
 * -> handler 拒绝策略，当线程无法执行新任务时（一般是由于线程池中的线程数量已经达到最大数或者线程池关闭导致的），
 * 默认情况下，当线程池无法处理新线程时，会抛出一个RejectedExecutionException。
 */
public class ThreadPool {
    private ThreadPoolExecutor mExecutor;
    private final int corePoolSize;
    private final int maximumPoolSize;
    private final long keepAliveTime;

    private ThreadPool(int corePoolSize, int
            maximumPoolSize, long keepAliveTime) {
        this.corePoolSize = corePoolSize;
        this.maximumPoolSize = maximumPoolSize;
        this.keepAliveTime = keepAliveTime;
    }

    public void executor(Runnable runnable) {
        if (runnable == null) {
            return;
        }
        //        再然后对ThreadPool内部类，在类里面对他实例化，实现单例
        if (mExecutor == null) {
            mExecutor = new
                    ThreadPoolExecutor(corePoolSize, //核心线程数
                    maximumPoolSize, //最大线程数
                    keepAliveTime, //闲置线程存活时间
                    TimeUnit.MILLISECONDS, // 时间单位
                    new LinkedBlockingDeque<Runnable>(),
                    //线程队列
                    Executors.defaultThreadFactory(),
                    //线程工厂
                    new ThreadPoolExecutor.AbortPolicy()
                    //队列已满，而且当前线程数已经超过最大线程数时的异常处理策略
            );
        }
        mExecutor.execute(runnable);
    }

    private static ThreadPool mThreadPool = null;

    // 获取单例的线程池对象
    public static ThreadPool getThreadPool() {
        if (mThreadPool == null) {
            synchronized (ThreadPool.class) {
                if (mThreadPool == null) {
                    int cpuNum =
                            Runtime.getRuntime().availableProcessors();// 获取处理器数量
                    int threadNum = cpuNum * 2 + 1;// 根据cpu数量, 计算出合理的线程并发数
                    mThreadPool = new
                            ThreadPool(threadNum, threadNum, 0L);
                }
            }
        }

        return mThreadPool;
    }

}
