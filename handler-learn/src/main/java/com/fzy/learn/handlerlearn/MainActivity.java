package com.fzy.learn.handlerlearn;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.widget.TextView;

import java.util.concurrent.ThreadPoolExecutor;

public class MainActivity extends AppCompatActivity {


    public static void startMainActivity(Context context) {
        Intent intent = new Intent(context, MainActivity.class);
        context.startActivity(intent);
    }


    /**
     * 这是当前推荐的写法 myLopper实际上是在 ThreadLocal<Looper> 中获取到的
     * 在里面也可以
     */
    Handler handler = new Handler(Looper.myLooper());

    StringBuilder stringBuilder = new StringBuilder();

    TextView mainLogTV ;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mainLogTV = findViewById(R.id.id_main_log_text);


//        MainActivity2.startMainActivity2(this);
//        this.startActivity(new Intent());
        // 我们常用的 几种sendMessage 实际上最终都会调用到 sendMessageAtTime
        // 然后将其 添加到 MessageQueue 中
//        handler.sendMessage(null);
        threadPoolTest();
    }
    public void showText(String needShowText){

        stringBuilder.append("--------------------------")
                .append("\n");

        handler.post(()->{
            mainLogTV.setText(stringBuilder);
        });

        for (int i = 0; i < 20; i++) {
            try {
                Thread.sleep(500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            stringBuilder.append(" ")
                    .append(needShowText)
                    .append(" 当前 条目")
                    .append(i)
                    .append("  \n");

            handler.post(()->{
                mainLogTV.setText(stringBuilder);
            });
        }

        stringBuilder.append("--------------------------")
                .append("\n");

        stringBuilder.append(" ")
                .append(needShowText)
                .append("结束")
                .append("  \n");

        handler.post(()->{
            mainLogTV.setText(stringBuilder);
        });
    }


    public void threadPoolTest() {
        for (int i = 0; i < 50; i++) {
            int finalI = i;


            ThreadPool.getThreadPool().executor(()->{
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                showText("当前执行 "+ finalI +"线程" );
            });
        }

    }
}